record("*", "$(P)$(R_LTC2991)-ValueV1_RBV") {
    alias("$(P)$(R_LTC2991)-9v5BusVol")

    field(DESC, "9.5V Bus Voltage")
    field(EGU,  "V")
    field(ASLO, "11.0")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-ValueV2_RBV") {
    alias("$(P)$(R_LTC2991)-15vBusVol")

    field(DESC, "15V Bus Voltage")
    field(EGU,  "V")
    field(ASLO, "11.0")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-ValueV4_RBV") {
    alias("$(P)$(R_LTC2991)-CompInVol")

    field(DESC, "Comparator in Voltage")
    field(EGU,  "V")
    field(ASLO, "2.0")

    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R_LTC2991)-ThrsCurr") {
    field(DESC, "Threshold Current")
    field(EGU,  "mA")
    field(CALC, "A*B")
    field(INPA, "$(P)$(R_LTC2991)-CompInVol CP")
    field(INPB, "2.325")
    field(HHSV, "MAJOR")
    field(HSV,  "MINOR")
    field(LSV,  "MINOR")
    field(LLSV, "MAJOR")

    info(ARCHIVE_THIS, "VAL LOW LOLO HIGH HIHI")
}

record("*", "$(P)$(R_LTC2991)-ValueV5_RBV") {
    alias("$(P)$(R_LTC2991)-5vBusVol")

    field(DESC, "5V Bus voltage")
    field(EGU,  "V")
    field(ASLO, "2.0")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-ValueV6_RBV") {
    alias("$(P)$(R_LTC2991)-AnaVol")

    field(DESC, "Analog voltage")
    field(EGU,  "V")
    field(ASLO, "2.0")

    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R_LTC2991)-LeakCurr") {
    field(DESC, "Leakage Current")
    field(EGU,  "mA")
    field(CALC, "A*B")
    field(INPA, "$(P)$(R_LTC2991)-AnaVol CP")
    field(INPB, "2.325")
    field(HHSV, "MAJOR")
    field(HSV,  "MINOR")
    field(LSV,  "MINOR")
    field(LLSV, "MAJOR")

    info(ARCHIVE_THIS, "VAL LOW LOLO HIGH HIHI")
}

record("*", "$(P)$(R_LTC2991)-Trigger") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_LTC2991)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record(dfanout, "$(P):LeakCurrHihi") {
    field(DESC, "Leakage Current high-high limit")
    field(VAL,  "$(LEAK_HIHI=100)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-LeakCurr.HIHI")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):LeakCurrHigh") {
    field(DESC, "Leakage Current high limit")
    field(VAL,  "$(LEAK_HIGH=50)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-LeakCurr.HIGH")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):LeakCurrLow") {
    field(DESC, "Leakage Current low limit")
    field(VAL,  "$(LEAK_LOW=0)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-LeakCurr.LOW")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):LeakCurrLolo") {
    field(DESC, "Leakage Current low-low limit")
    field(VAL,  "$(LEAK_LOLO=0)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-LeakCurr.LOLO")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):ThrsCurrHigh") {
    field(DESC, "Threshold Curr high limit")
    field(VAL,  "$(THRS_HIGH=50)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-ThrsCurr.HIGH")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):ThrsCurrHihi") {
    field(DESC, "Threshold Curr high-high limit")
    field(VAL,  "$(THRS_HIHI=50)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-ThrsCurr.HIHI")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):ThrsCurrLow") {
    field(DESC, "Threshold Curr low limit")
    field(VAL,  "$(THRS_LOW=0)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-ThrsCurr.LOW")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record(dfanout, "$(P):ThrsCurrLolo") {
    field(DESC, "Threshold Curr low-low limit")
    field(VAL,  "$(THRS_LOLO=0)")
    field(EGU,  "mA")
    field(OUTA, "$(P)$(R_LTC2991)-ThrsCurr.LOLO")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TMP100)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_TMP100)-Value_RBV") {
    field(EGU,  "C")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_TCA9555)-LevelPin7") {
    alias("$(P)$(R_TCA9555)-LEDControl")

    field(DESC, "Green LED on e_pickup front panel")
    field(ZNAM, "OFF")
    field(ONAM, "ON")

    info(autosaveFields, "VAL")
}

record("*", "$(P)$(R_TCA9555)-LevelPin7_RBV") {
    alias("$(P)$(R_TCA9555)-LEDStatus")

    field(DESC, "Green LED on e_pickup front panel")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record("*", "$(P)$(R_TCA9555)-DirPin7") {
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-LevelPin0_RBV") {
    alias("$(P)$(R_TCA9555)-FiberTestMon")

    field(DESC, "Fiber Test Monitoring")
    field(ZNAM, "Testing")
    field(ONAM, "OK")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-DirPin0") {
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-LevelPin1_RBV") {
    alias("$(P)$(R_TCA9555)-IntlkOFF")

    field(DESC, "Interlock OFF")
    field(ZNAM, "NOK")
    field(ONAM, "OK")
}

record("*", "$(P)$(R_TCA9555)-DirPin1") {
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-LevelPin2_RBV") {
    alias("$(P)$(R_TCA9555)-IntlkON")

    field(DESC, "Interlock ON")
    field(ZNAM, "OK")
    field(ONAM, "NOK")
    field(OSV,  "MAJOR")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-DirPin2") {
    info(ARCHIVE_THIS, "")
}

record(calcout,  "$(P):#IntlkInvalidCalc") {
    field(DESC, "Calc if Interlock signals are valid")
    field(CALC, "A==B")
    field(INPA, "$(P)$(R_TCA9555)-IntlkON CPP")
    field(INPB, "$(P)$(R_TCA9555)-IntlkOFF NPP")
    field(OUT,  "$(P):IntlkInvalid PP MS")
    field(OOPT, "Every Time")
    field(PINI, "YES")
}

record(bi,  "$(P):IntlkInvalid") {
    field(DESC, "Interlock signals are valid")
    field(ONAM, "NOK")
    field(ZNAM, "OK")
    field(OSV,  "MAJOR")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_AD527X)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_AD527X)-Value_RBV") {
    field(DESC, "Resistance readback")
    field(EGU,  "Ohm")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_AD527X)-Value") {
    field(DESC, "Resistance setpoint")
    field(EGU,  "Ohm")

    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
}
